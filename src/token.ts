import {AuthenticationClient} from 'auth0'
import Cache from 'hybrid-disk-cache'
import {homedir} from 'os'
import {join} from 'path'

const cache = new Cache({path: join(homedir(), '.any4'), tbd: 10})
const {ANY4_TOKEN, ANY4_CLIENT, ANY4_SECRET, ANY4_USERNAME, ANY4_PASSWORD} = process.env
const domain = 'any4.auth0.com'

export default async function ({token = ANY4_TOKEN, client = ANY4_CLIENT, secret = ANY4_SECRET, username = ANY4_USERNAME, password = ANY4_PASSWORD}) {
  if (token) return token
  let access_token, expires_in
  if (client) {
    if (cache.has(client) === 'hit') return cache.get(client).toString('utf-8')
    else ({access_token, expires_in} = await new AuthenticationClient({domain, clientId: client, clientSecret: secret}).clientCredentialsGrant({audience: 'https://any4.io/'}))
  } else if (username) {
    if (cache.has(username) === 'hit') return cache.get(username).toString('utf-8')
    else ({access_token, expires_in} = await new AuthenticationClient({domain}).passwordGrant({username, password}))
  }

  if (!access_token) throw  new Error('Could not obtain an access token, please specify credentials')
  cache.set(client, Buffer.from(access_token, 'utf-8'), expires_in)
  return access_token
}
