#!/usr/bin/env node --unhandled-rejections=strict

import {Command} from 'commander'
import projects from './projects'

const program = new Command()

program
  .option('--token', 'Authorization token, may also be specified via environment variable ANY4_TOKEN. If both the command line value and environment variable are specified the command line value takes precedence. Takes precedence over any other credentials present.')
  .option('--client', 'Client ID, may also be specified via environment variable ANY4_CLIENT. If both the command line value and environment variable are specified the command line value takes precedence. Takes precedence over username credentials.')
  .option('--secret', 'Client secret, may also be specified via environment variable ANY4_SECRET. If both the command line value and environment variable are specified the command line value takes precedence. Takes precedence over username credentials.')
  .option('--username', 'Username, may also be specified via environment variable ANY4_USERNAME. If both the command line value and environment variable are specified the command line value takes precedence.')
  .option('--password', 'Password, may also be specified via environment variable ANY4_PASSWORD. If both the command line value and environment variable are specified the command line value takes precedence.')

projects(program)

program.parse(process.argv)
