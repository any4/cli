import {Command} from 'commander'
import token from './token'
import {projects} from '@any4/sdk'

export default function (program: Command): void {
  program
    .command('create project <name>')
    .description('Create a new project')
    .action(async name => projects.create({bearer: await token(program as any), name}))
}
